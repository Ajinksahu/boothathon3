function point1(){
    //point A
    let X1:HTMLInputElement=<HTMLInputElement>document.getElementById("t1");
    let Y1:HTMLInputElement=<HTMLInputElement>document.getElementById("t2");
    var x1:number=parseFloat(X1.value);
    var y1:number=parseFloat(Y1.value);
    //point B
    let X2:HTMLInputElement=<HTMLInputElement>document.getElementById("t3");
    let Y2:HTMLInputElement=<HTMLInputElement>document.getElementById("t4");
    var x2:number=parseFloat(X2.value);
    var y2:number=parseFloat(Y2.value);
    //point C
    let X3:HTMLInputElement=<HTMLInputElement>document.getElementById("t5");
    let Y3:HTMLInputElement=<HTMLInputElement>document.getElementById("t6");
    var x3:number=parseFloat(X3.value);
    var y3:number=parseFloat(Y3.value);
    // any point point P
    let X4:HTMLInputElement=<HTMLInputElement>document.getElementById("t7");
    let Y4:HTMLInputElement=<HTMLInputElement>document.getElementById("t8");
    var x4:number=parseFloat(X4.value);
    var y4:number=parseFloat(Y4.value);
  
     //entered values are not a no.
    if(isNaN(x1) || isNaN(y1) || isNaN(x2) || isNaN(y2) || isNaN(x3) || isNaN(y3) || isNaN(x4) || isNaN(y4)){
        if(isNaN(x1)){
            alert("Entered value of x1 is not a number ");
        }if(isNaN(y1)){
            alert("Entered value of y1 is not a number ");
        }if(isNaN(x2)){
            alert("Entered value of x2 is not a number ");
        }if(isNaN(y2)){
            alert("Entered value of y2 is not a number ");
        }if(isNaN(x3)){
            alert("Entered value of x3 is not a number ");
        }if(isNaN(y3)){
            alert("Entered value of y3 is not a number ");
        }if(isNaN(x4)){
            alert("Entered value of x4 is not a number ");
        }if(isNaN(y4)){
            alert("Entered value of y4 is not a number ");
        }
    }
    else{  //entered values are no.
        var ABC = (x1*(y2-y3)+x2*(y3-y1)+x3*(y1-y2))/2;
        var PCA = (x1*(y4-y3)+x3*(y1-y4)+x4*(y3-y1))/2;
        var PBC = (x3*(y4-y2)+x2*(y3-y4)+x4*(y2-y3))/2;
        var PAB = (x1*(y2-y4)+x2*(y4-y1)+x4*(y1-y2))/2;
         //whether the point p lies inside or outside the triangle
        var sum = Math.abs(PCA)+Math.abs(PBC)+Math.abs(PAB);
        var A = Math.abs(ABC)-sum;
        console.log(sum,A)
        if(A==0){ 
            document.getElementById("p").innerHTML="The Point P lies in the triangle ABC";
        }
        else{
            document.getElementById("p").innerHTML="The Point P don't lie in the triangle ABC";
        } 
    }
}