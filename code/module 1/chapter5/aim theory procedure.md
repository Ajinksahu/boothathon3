# Newton's Ring Experiment
------
> - ## <i> Aim: </i>
 ***The aim of the experiment is to determine wavelength of light and refractive index using Newton's ring experiment.***
 
 >- ## <i> Theory: </i>
 ## <span style="color:blue">Introduction: </span>
***In 1717, Sir Isaac Newton studied the rings pattern generated due to interference of light. These rings are known as Newton’s rings. They arise from the interference of light. The phenomenon of interference of light waves is obtained from monochromatic and coherent rays i.e. rays of same frequency and constant phase difference. Newton's ring pattern is a result of interference between the partially reflected and partially transmitted rays from the lower curved surface of plano-convex lens and upper surface of the plane glass plate. When viewed with a monochromatic source, it appears as a series of concentric, alternate bright and dark rings centred at the point of contact between the two surfaces. These rings appear in the field of view of telescope. The wavelength of the monochromatic light and the refractive index of a given transparent liquid medium present in the wedge-shaped film can be calculated by studying the ring pattern.***
## <span style="color:blue">Concept: <span>
***The redistribution of energy by superposition of light waves is called as interference. When two or more waves superimpose over each other, resultant intensity is modified. The modification in the distribution of intensity in the region of superposition is called as interference. Interference fringes are alternately bright and dark patches of light obtained in the region of superposition. There is no loss of energy in interference phenomenon, only redistribution of energy takes place. The energy absent at dark places is actually present in bright regions. Alternate dark and bright rings formed due to presence of air film when plano-convex lens is placed on glass plate are called Newton’s rings. When a plano-convex surface is placed on a glass plate, an air film of gradually increasing thickness is formed. The thickness of the air film is symmetrical and increases outwards from the point of contact. When monochromatic light is allowed to fall normally on this air film and viewed in reflected light, alternate dark and bright rings are observed. The rings are formed as a result of interference between light waves reflected from the upper and lower surfaces of the air film developed between the convex surface of plano convex lens and plane glass plate. This is so because the air film formed is wedge shaped and loci of points of equal thickness of air film are circles concentric with point of contact. If 't' is the thickness of the air film at a point on the film, the refracted wavelet from the lens has to travel a distance 't' into the film, and after reflection from the top surface of the glass plate, it has to travel the same distance back to reach the point again. Thus, it travels a total path '2t'. One of the two reflections takes place at the surface of the denser medium and hence it introduces an additional phase change of π or an equivalent path difference λ/2 between two wavelets.
<br/>The centre of the ring dark in Newton’s Rings experiment with reflected light is dark because at the point of contact the path difference is zero but one of the interfering ray is reflected so the effective path difference becomes λ/2 thus the condition of minimum intensity is created hence centre of ring pattern is dark.
<br/>The corresponding ray diagram is as shown in Figure 1.***
![diagram](raydiagram.jpg "ray diagram")
<br/> 
***Figure 1 Newton's ring ray diagram.
The wavelength of the light can be calculated using the formula***
<span style="color:red"> λ=Dn^2−Dm^22/4(n−m)R </span>

***Where <span style="color:red">Dn</span>  &<span style="color:red">Dm</span>  are Diameters.
 <br/><span style="color:red">n</span>& <span style="color:red">m</span> are Ring No.
 <br/><span style="color:red">R</span>is the radius of planoconvex lens.***

 >- ## <i> Procedure: </i>
### <span style="color:blue">These procedure steps will be followed on the simulator </span>
<br>***1. After going through the theory and pretest, click the "Simulation" tab
<br/>2. The simulator will display the interactive questions, attempt the questions***
![diagram](pic1.png)<br/>
***3. Click "Submit" button to verify the answers given, click on simulator button to skip the test and go to simulator.***
![diagram](pic2.png )<br/>
***4. Select a medium by clicking on the container symbols***
![diagram](pic3.png )<br/>
***5. Click on "Show" button. This will reveal the apparatus.***
![diagram](pic4.jpg )<br/>
***6. Click on "Switch" button. This will show the light source is on. Note the rings pattern appearing in the field of view of telescope seen at the right top of the screen***
![diagram](pic5.jpg )<br/>
***7. Use microscope controls to observe the motion of travelling microscope.***
![diagram](pic6.jpg )<br/>
***8. Click on "Next" button to go to the next page of simulator.***
![diagram](pic7.jpg )<br/>
***9. The simulator will display the interactive questions, attempt the questions
<br/>10. Click on "Show Travelling Microscope" button. This will reveal the experimental setup.***
![diagram](pic8.png )<br/>
***11. Use Microscope movement and lens movement buttons to get resolved ring pattern and set the vertical cross wire at appropriate position.***
![diagram](pic9.png )<br/>
***12. To note the readings click on "Add to table" button.
<br/>13. To show the graph, click on "Generate graph" button.
<br/>14. Click on "Conclusion" button
<br/>15. The simulator will display the interactive questions, attempt the questions.
<br/>16. Note the conclusions from the experiment performed.***